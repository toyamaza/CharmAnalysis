/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/// @author Miha Muskinja

//
// includes
//

#include <xAODBase/IParticle.h>
#include <xAODTracking/Vertex.h>
#include <xAODTracking/VertexContainer.h>

#include <CharmAnalysis/EventInfoCreatorAlg.h>

//
// method implementations
//

// compare pT of two IParticle objects
bool compare_pt(const xAOD::IParticle *p1, const xAOD::IParticle *p2) {
    return (p1->pt() > p2->pt());
}

EventInfoCreatorAlg ::EventInfoCreatorAlg(const std::string &name,
                                          ISvcLocator *pSvcLocator)
    : AnaAlgorithm(name, pSvcLocator),
    m_enableFilter(false),
    m_Nlep(0)
{
    declareProperty("enableFilter", m_enableFilter);
    declareProperty("Nlep", m_Nlep);
    declareProperty("doTruth", m_doTruth);
    declareProperty("pileupRW", m_pileupRW);
    declareProperty("leptonInfo", m_leptonInfo);
    declareProperty("primaryVertexKey", m_primaryVertexKey);
    declareProperty("passSingleElectronTriggerChainDecoration", m_passSingleElectronTriggerChain);
    declareProperty("passSingleMuonTriggerChainDecoration", m_passSingleMuonTriggerChain);
    declareProperty("EventWeightDecoration", m_EventWeight);
    declareProperty("TightElPtCut", m_tight_el_pt_cut);
    declareProperty("TightMuPtCut", m_tight_mu_pt_cut);
    declareProperty("TightElId", m_tight_el_id);
    declareProperty("TightElIso", m_tight_el_iso);
    declareProperty("TightMuQuality", m_tight_mu_quality);
    declareProperty("TightMuIso", m_tight_mu_iso);
}

StatusCode EventInfoCreatorAlg ::initialize() {
    if (!m_passSingleElectronTriggerChain.empty()) {
        m_passSingleElectronTriggerChainAccessor =
            std::make_unique<SG::AuxElement::Accessor<bool>>(
                m_passSingleElectronTriggerChain);
    }
    if (!m_passSingleMuonTriggerChain.empty()) {
        m_passSingleMuonTriggerChainAccessor =
            std::make_unique<SG::AuxElement::Accessor<bool>>(
                m_passSingleMuonTriggerChain);
    }
    if (!m_EventWeight.empty()) {
        m_EventWeightAccessor =
            std::make_unique<SG::AuxElement::Accessor<float>>(m_EventWeight);
    }

    // Top reweighter for nominal ttbar sample
    m_top_reweighter = new TTbarNNLOReweighter(410470);
    m_top_reweighter->Init();

    // Primary Vertex
    m_primaryVertex_x_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("PV_X");
    m_primaryVertex_y_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("PV_Y");
    m_primaryVertex_z_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("PV_Z");

    // BeamSpot
    m_BS_posx_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("beamPosX");
    m_BS_posy_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("beamPosY");
    m_BS_posz_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("beamPosZ");
    m_BS_possigmax_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("beamPosSigmaX");
    m_BS_possigmay_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("beamPosSigmaY");
    m_BS_possigmaz_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("beamPosSigmaZ");

    // truth V boson
    m_truth_boson_m_accessor = std::make_unique<SG::AuxElement::Accessor<float>>("TRUTH_V_M");

    // pass OR accessor
    ANA_CHECK(makeSelectionReadAccessor(m_OR_selectionDecoration, m_OR_selectionAccessor, true));

    m_tight_el_id_decoration = "likelihood_" + m_tight_el_id;
    m_tight_el_iso_decoration = "isIsolated_" + m_tight_el_iso;

    // tight electron selection
    ANA_CHECK(makeSelectionReadAccessor(m_tight_el_id_decoration,
                                    m_tight_el_id_accessor, true));
    ANA_CHECK(makeSelectionReadAccessor(m_tight_el_iso_decoration,
                                    m_tight_el_iso_accessor, true));

    m_el_id_tight_sf = "effSF_ID_" + m_tight_el_id + "_NOSYS";
    m_el_iso_tight_sf = "effSF_Isol_" + m_tight_el_id + "_" + m_tight_el_iso + "_NOSYS";
    m_el_tight_trig_sf =
        "effSF_Trig_" + m_tight_el_id + "_" + m_tight_el_iso + "_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_"
        "lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
        "lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS";
    m_el_tight_trig_eff =
        "effSF_Trig_" + m_tight_el_id + "_" + m_tight_el_iso + "_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_"
        "e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_"
        "e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS";

    m_tight_mu_quality_decoration = "isQuality_" + m_tight_mu_quality;
    m_tight_mu_iso_decoration = "isIsolated_" + m_tight_mu_iso;

    // tight muon selection
    ANA_CHECK(makeSelectionReadAccessor(m_tight_mu_quality_decoration,
                                    m_tight_mu_quality_accessor, true));
    ANA_CHECK(makeSelectionReadAccessor(m_tight_mu_iso_decoration,
                                    m_tight_mu_iso_accessor, true));

    m_mu_quality_tight_sf = "muon_effSF_Quality_" + m_tight_mu_quality + "_NOSYS";
    m_mu_iso_tight_sf = "muon_effSF_Isol_" + m_tight_mu_iso + "_NOSYS";
    m_mu_tight_trig_mc_2015_eff =
        "muon_effMC_Trig_" + m_tight_mu_quality + "_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS";
    m_mu_tight_trig_data_2015_eff =
        "muon_effData_Trig_" + m_tight_mu_quality + "_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS";
    m_mu_tight_trig_mc_2016_eff =
        "muon_effMC_Trig_" + m_tight_mu_quality + "_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS";
    m_mu_tight_trig_data_2016_eff =
        "muon_effData_Trig_" + m_tight_mu_quality + "_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS";

    // various accessors
    initializeAccessors();

    ANA_CHECK(m_eventInfoHandle.initialize(m_systematicsList));
    ANA_CHECK(m_truthLeptons.initialize(m_systematicsList));
    if (m_leptonInfo) {
        ANA_CHECK(m_electronsHandle.initialize(m_systematicsList));
        ANA_CHECK(m_muonsHandle.initialize(m_systematicsList));
    }
    ANA_CHECK(m_systematicsList.initialize());
    return StatusCode::SUCCESS;
}

StatusCode EventInfoCreatorAlg ::execute() {
    for (const auto &sys : m_systematicsList.systematicsVector())
    {
        // Primary vertex
        const xAOD::Vertex *primaryVertex = nullptr;
        const xAOD::VertexContainer *primaryVertices = nullptr;
        CHECK(evtStore()->retrieve(primaryVertices, m_primaryVertexKey));
        for (const xAOD::Vertex *vertex : *primaryVertices) {
            if (vertex->vertexType() == xAOD::VxType::PriVtx) {
                // The default "PrimaryVertex" container is ordered in
                // sum-pt, and the tracking group recommends to pick the one
                // with the maximum sum-pt, so this will do the right thing.
                // If the user needs a different primary vertex, he needs to
                // provide a reordered primary vertex container and point
                // this algorithm to it. Currently there is no central
                // algorithm to do that, so users will have to write their
                // own (15 Aug 18).
                if (primaryVertex == nullptr) {
                    primaryVertex = vertex;
                    break;
                }
            }
        }
        if (!primaryVertex) {
            ATH_MSG_WARNING("No primary vertex found");
            // return StatusCode::SUCCESS;
        }

        // charm event info
        auto charmInfo = std::make_unique<xAOD::EventInfo>();
        auto charmInfo_store = std::make_unique<xAOD::EventAuxInfo>();
        charmInfo->setStore(charmInfo_store.get());
        fillAccessors(charmInfo.get());

        // common eventInfo object
        const xAOD::EventInfo *eventInfo;
        ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

        // run number
        unsigned int runNumber;
        if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
            runNumber = eventInfo->auxdecor<unsigned int>("RandomRunNumber");
            if (m_pileupRW) {
                // MC event weight and JVT weight
                (*m_EventWeightAccessor)(*charmInfo) =
                    eventInfo->auxdecor<float>("PileupWeight") *
                    eventInfo->auxdecor<float>("jvt_effSF_NOSYS") *
                    eventInfo->auxdecor<float>("fjvt_effSF_NOSYS") *
                    eventInfo->mcEventWeights().at(0);
            } else {
                // MC event weight
                (*m_EventWeightAccessor)(*charmInfo) =
                    eventInfo->mcEventWeights().at(0);
            }
        } else {
            runNumber = eventInfo->runNumber();
            (*m_EventWeightAccessor)(*charmInfo) = 1;
        }

        // primary vertex
        if (primaryVertex) {
            (*m_primaryVertex_x_accessor)(*charmInfo) = primaryVertex->x();
            (*m_primaryVertex_y_accessor)(*charmInfo) = primaryVertex->y();
            (*m_primaryVertex_z_accessor)(*charmInfo) = primaryVertex->z();
        }
        else {
            (*m_primaryVertex_x_accessor)(*charmInfo) = 0;
            (*m_primaryVertex_y_accessor)(*charmInfo) = 0;
            (*m_primaryVertex_z_accessor)(*charmInfo) = 0;
        }

        // beamspt
        (*m_BS_posx_accessor)(*charmInfo) = eventInfo->beamPosX();
        (*m_BS_posy_accessor)(*charmInfo) = eventInfo->beamPosY();
        (*m_BS_posz_accessor)(*charmInfo) = eventInfo->beamPosZ();
        (*m_BS_possigmax_accessor)(*charmInfo) = eventInfo->beamPosSigmaX();
        (*m_BS_possigmay_accessor)(*charmInfo) = eventInfo->beamPosSigmaY();
        (*m_BS_possigmaz_accessor)(*charmInfo) = eventInfo->beamPosSigmaZ();

        // truth V boson
        // TODO: make this ugly convoluted code better
        // issue is that in W->taunu events we get two W bosons,
        // but only the one decaying to tau nu is correct
        float truth_v_mass = -999;
        if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && m_doTruth) {
            const xAOD::TruthParticleContainer *turth_leptons = nullptr;
            ANA_CHECK(m_truthLeptons.retrieve(turth_leptons, sys));
            if (turth_leptons->size() > 1) {
                TLorentzVector v_boson;
                std::vector<int> truth_barcodes;
                for (auto *truth : *turth_leptons) {
                    truth_barcodes.push_back(truth->barcode());
                }
                // get the two particles with lowest barcode
                std::sort(truth_barcodes.begin(), truth_barcodes.end());
                assert(truth_barcodes[0] < truth_barcodes[1]);
                for (auto *truth : *turth_leptons) {
                    if (truth->barcode() == truth_barcodes[0]) {
                        v_boson += truth->p4();
                    } else if (truth->barcode() == truth_barcodes[1]) {
                        v_boson += truth->p4();
                    }
                }
                truth_v_mass = v_boson.M();
            }
        }
        (*m_truth_boson_m_accessor)(*charmInfo) = truth_v_mass;

        // top quark pT reweight
        if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && eventInfo->mcChannelNumber() == 410470) {
            float top_pt = -1;
            const xAOD::TruthParticleContainer *truthParticles;
            ANA_CHECK(evtStore()->retrieve(truthParticles, "TruthParticles"));
            for (auto *p : *truthParticles) {
                if (p->pdgId() == 6 && p->status() == 62) {
                    top_pt = p->pt() / CLHEP::GeV;
                }
            }
            (*m_top_weight_accessor)(*charmInfo) = m_top_reweighter->GetTopPt_Powheg_Pythia8_Nominal(top_pt);
        } else {
            (*m_top_weight_accessor)(*charmInfo) = 1.0;
        }

        // event trigger pass
        bool passMuonChain = false;
        bool passElectronChain = false;
        // 2015 only
        if (runNumber < 290000) {
            passMuonChain =
                eventInfo->auxdecor<bool>("trigPassed_HLT_mu50") ||
                eventInfo->auxdecor<bool>("trigPassed_HLT_mu20_iloose_L1MU15");
            passElectronChain =
                eventInfo->auxdecor<bool>("trigPassed_HLT_e24_lhmedium_L1EM20VH") ||
                eventInfo->auxdecor<bool>("trigPassed_HLT_e60_lhmedium") ||
                eventInfo->auxdecor<bool>("trigPassed_HLT_e120_lhloose");
        }
        // 2016-2018
        else {
            passMuonChain =
                eventInfo->auxdecor<bool>("trigPassed_HLT_mu50") ||
                eventInfo->auxdecor<bool>("trigPassed_HLT_mu26_ivarmedium");
            passElectronChain =
                eventInfo->auxdecor<bool>("trigPassed_HLT_e26_lhtight_nod0_ivarloose") ||
                eventInfo->auxdecor<bool>("trigPassed_HLT_e60_lhmedium_nod0") ||
                eventInfo->auxdecor<bool>("trigPassed_HLT_e140_lhloose_nod0");
        }
        (*m_passSingleMuonTriggerChainAccessor)(*charmInfo) = passMuonChain;
        (*m_passSingleElectronTriggerChainAccessor)(*charmInfo) = passElectronChain;

        // lepton containers
        std::vector<const xAOD::Electron *> tight_electrons;
        std::vector<const xAOD::Muon *> tight_muons;

        if (m_leptonInfo) {
            // Electron and Muon containers
            const xAOD::ElectronContainer *electrons;
            const xAOD::MuonContainer *muons;
            ANA_CHECK(m_electronsHandle.retrieve(electrons, sys));
            ANA_CHECK(m_muonsHandle.retrieve(muons, sys));

            // tight electrons
            for (auto *el : *electrons) {
                if (el->pt() < m_tight_el_pt_cut)
                    continue;
                if (!m_tight_el_id_accessor->getBool(*el))
                    continue;
                if (!m_tight_el_iso_accessor->getBool(*el))
                    continue;
                if (!m_OR_selectionAccessor->getBool(*el))
                    continue;
                tight_electrons.push_back(el);
            }

            // tight muons
            for (auto *mu : *muons) {
                if (mu->pt() < m_tight_mu_pt_cut)
                    continue;
                if (!m_tight_mu_quality_accessor->getBool(*mu))
                    continue;
                if (!m_tight_mu_iso_accessor->getBool(*mu))
                    continue;
                if (!m_OR_selectionAccessor->getBool(*mu))
                    continue;
                tight_muons.push_back(mu);
            }

            // sort vectors
            std::sort(tight_electrons.begin(), tight_electrons.end(), compare_pt);
            std::sort(tight_muons.begin(), tight_muons.end(), compare_pt);

            // fill to branches
            if (tight_electrons.size() > 0) {
                (*m_tight_el1_pt_accessor)(*charmInfo) =
                    tight_electrons.at(0)->pt();
                (*m_tight_el1_phi_accessor)(*charmInfo) =
                    tight_electrons.at(0)->phi();
                (*m_tight_el1_eta_accessor)(*charmInfo) =
                    tight_electrons.at(0)->eta();
                (*m_tight_el1_charge_accessor)(*charmInfo) =
                    tight_electrons.at(0)->charge();
                for (unsigned int i = 0; i < electrons->size(); i++) {
                    if (tight_electrons.at(0) == electrons->at(i))
                        (*m_tight_el1_index_accessor)(*charmInfo) = i;
                }
                (*m_tight_el1_matched_accessor)(*charmInfo) = el_is_trigger_matched(
                    tight_electrons.at(0), eventInfo, runNumber);
            }
            if (tight_electrons.size() > 1) {
                (*m_tight_el2_pt_accessor)(*charmInfo) =
                    tight_electrons.at(1)->pt();
                (*m_tight_el2_phi_accessor)(*charmInfo) =
                    tight_electrons.at(1)->phi();
                (*m_tight_el2_eta_accessor)(*charmInfo) =
                    tight_electrons.at(1)->eta();
                (*m_tight_el2_charge_accessor)(*charmInfo) =
                    tight_electrons.at(1)->charge();
                for (unsigned int i = 0; i < electrons->size(); i++) {
                    if (tight_electrons.at(1) == electrons->at(i))
                        (*m_tight_el2_index_accessor)(*charmInfo) = i;
                }
                (*m_tight_el2_matched_accessor)(*charmInfo) = el_is_trigger_matched(
                    tight_electrons.at(1), eventInfo, runNumber);
            }
            if (tight_muons.size() > 0) {
                (*m_tight_mu1_pt_accessor)(*charmInfo) = tight_muons.at(0)->pt();
                (*m_tight_mu1_phi_accessor)(*charmInfo) = tight_muons.at(0)->phi();
                (*m_tight_mu1_eta_accessor)(*charmInfo) = tight_muons.at(0)->eta();
                (*m_tight_mu1_charge_accessor)(*charmInfo) =
                    tight_muons.at(0)->charge();
                for (unsigned int i = 0; i < muons->size(); i++) {
                    if (tight_muons.at(0) == muons->at(i))
                        (*m_tight_mu1_index_accessor)(*charmInfo) = i;
                }
                (*m_tight_mu1_matched_accessor)(*charmInfo) =
                    mu_is_trigger_matched(tight_muons.at(0), eventInfo, runNumber);
            }
            if (tight_muons.size() > 1) {
                (*m_tight_mu2_pt_accessor)(*charmInfo) = tight_muons.at(1)->pt();
                (*m_tight_mu2_phi_accessor)(*charmInfo) = tight_muons.at(1)->phi();
                (*m_tight_mu2_eta_accessor)(*charmInfo) = tight_muons.at(1)->eta();
                (*m_tight_mu2_charge_accessor)(*charmInfo) =
                    tight_muons.at(1)->charge();
                for (unsigned int i = 0; i < muons->size(); i++) {
                    if (tight_muons.at(1) == muons->at(i))
                        (*m_tight_mu2_index_accessor)(*charmInfo) = i;
                }
                (*m_tight_mu2_matched_accessor)(*charmInfo) =
                    mu_is_trigger_matched(tight_muons.at(1), eventInfo, runNumber);
            }

            // scale factors
            if (!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
                (*m_tight_el_weight_accessor)(*charmInfo) = 1;
                (*m_tight_mu_weight_accessor)(*charmInfo) = 1;
            } else {
                // one tight electron
                if (tight_electrons.size() == 1) {
                    (*m_tight_el_weight_accessor)(*charmInfo) = m_pileupRW
                        ? get_single_tight_el_weight(tight_electrons.at(0))
                        : 1;
                }
                // one tight muon
                if (tight_muons.size() == 1) {
                    (*m_tight_mu_weight_accessor)(*charmInfo) = m_pileupRW
                        ? get_single_tight_mu_weight(tight_muons.at(0), runNumber)
                        : 1;
                }
                // two tight electrons
                if (tight_electrons.size() == 2) {
                    (*m_tight_el_weight_accessor)(*charmInfo) = m_pileupRW
                        ? get_double_tight_el_weight(tight_electrons.at(0),
                                                     tight_electrons.at(1))
                        : 1;
                }
                // two tight muons
                if (tight_muons.size() == 2) {
                    (*m_tight_mu_weight_accessor)(*charmInfo) = m_pileupRW
                        ? get_double_tight_mu_weight(tight_muons.at(0),
                                                     tight_muons.at(1), runNumber)
                        : 1;
                }
            }
        }

        // record the new EventInfo object
        ANA_CHECK(m_eventInfoHandle.record(std::move(charmInfo), std::move(charmInfo_store), sys));

        // lepton number cut
        if (m_enableFilter && m_Nlep > 0 && (tight_muons.size() + tight_electrons.size())!=m_Nlep) {
            ATH_MSG_VERBOSE("Number of tight leptons does not match. Not passing the event.");
            setFilterPassed(false);
            return StatusCode::SUCCESS;
        }
    }
    return StatusCode::SUCCESS;
}

bool EventInfoCreatorAlg ::el_is_trigger_matched(
    const xAOD::Electron *el, const xAOD::EventInfo *eventInfo,
    unsigned int runNumber) {
    bool matched = false;
    if (runNumber < 290000) {
        matched = (eventInfo->auxdecor<bool>("trigPassed_HLT_e24_lhmedium_L1EM20VH") &&
                   el->auxdecor<bool>("matched_HLT_e24_lhmedium_L1EM20VH")) ||
                  (eventInfo->auxdecor<bool>("trigPassed_HLT_e60_lhmedium") &&
                   el->auxdecor<bool>("matched_HLT_e60_lhmedium")) ||
                  (eventInfo->auxdecor<bool>("trigPassed_HLT_e120_lhloose") &&
                   el->auxdecor<bool>("matched_HLT_e120_lhloose"));
    } else {
        matched =
            (eventInfo->auxdecor<bool>("trigPassed_HLT_e26_lhtight_nod0_ivarloose") &&
             el->auxdecor<bool>("matched_HLT_e26_lhtight_nod0_ivarloose")) ||
            (eventInfo->auxdecor<bool>("trigPassed_HLT_e60_lhmedium_nod0") &&
             el->auxdecor<bool>("matched_HLT_e60_lhmedium_nod0")) ||
            (eventInfo->auxdecor<bool>("trigPassed_HLT_e140_lhloose_nod0") &&
             el->auxdecor<bool>("matched_HLT_e140_lhloose_nod0"));
    }
    return matched;
}
bool EventInfoCreatorAlg ::mu_is_trigger_matched(
    const xAOD::Muon *mu, const xAOD::EventInfo *eventInfo,
    unsigned int runNumber) {
    bool matched = false;
    if (runNumber < 290000) {
        matched =
            (eventInfo->auxdecor<bool>("trigPassed_HLT_mu50") &&
             mu->auxdecor<bool>("matched_HLT_mu50")) ||
            (eventInfo->auxdecor<bool>("trigPassed_HLT_mu20_iloose_L1MU15") &&
             mu->auxdecor<bool>("matched_HLT_mu20_iloose_L1MU15"));
    } else {
        matched =
            (eventInfo->auxdecor<bool>("trigPassed_HLT_mu50") &&
             mu->auxdecor<bool>("matched_HLT_mu50")) ||
            (eventInfo->auxdecor<bool>("trigPassed_HLT_mu26_ivarmedium") &&
             mu->auxdecor<bool>("matched_HLT_mu26_ivarmedium"));
    }
    return matched;
}

float EventInfoCreatorAlg ::get_single_tight_el_weight(
    const xAOD::Electron *el) {
    float w = (*m_el_eff_sf_accessor)(*el) * (*m_el_id_tight_sf_accessor)(*el) *
              (*m_el_iso_tight_sf_accessor)(*el) *
              (*m_el_tight_trig_sf_accessor)(*el);
    return w;
}

float EventInfoCreatorAlg ::get_single_tight_mu_weight(const xAOD::Muon *mu,
                                                       unsigned int runNumber) {
    float w = (*m_mu_ttva_sf_accessor)(*mu) *
              (*m_mu_quality_tight_sf_accessor)(*mu) *
              (*m_mu_iso_tight_sf_accessor)(*mu);

    if (runNumber < 290000) {
        float mc_eff = (*m_mu_tight_trig_mc_2015_eff_accessor)(*mu);
        if (mc_eff <= 0)
            return 0;
        float data_eff = (*m_mu_tight_trig_data_2015_eff_accessor)(*mu);
        w *= data_eff / mc_eff;
    } else {
        float mc_eff = (*m_mu_tight_trig_mc_2016_eff_accessor)(*mu);
        if (mc_eff <= 0)
            return 0;
        float data_eff = (*m_mu_tight_trig_data_2016_eff_accessor)(*mu);
        w *= data_eff / mc_eff;
    }

    return w;
}

float EventInfoCreatorAlg ::get_double_tight_el_weight(
    const xAOD::Electron *el1, const xAOD::Electron *el2) {
    float w =
        (*m_el_eff_sf_accessor)(*el1) * (*m_el_id_tight_sf_accessor)(*el1) *
        (*m_el_iso_tight_sf_accessor)(*el1) * (*m_el_eff_sf_accessor)(*el2) *
        (*m_el_id_tight_sf_accessor)(*el2) *
        (*m_el_iso_tight_sf_accessor)(*el2);

    float effData1 = (*m_el_tight_trig_eff_accessor)(*el1);
    float effData2 = (*m_el_tight_trig_eff_accessor)(*el2);
    float effMC1 = (1. / effData1) * (*m_el_tight_trig_sf_accessor)(*el1);
    float effMC2 = (1. / effData2) * (*m_el_tight_trig_sf_accessor)(*el2);

    float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
    float PData = effData1 * effData2 + effData1 * (1 - effData2) +
                  (1 - effData1) * effData2;

    if (PMC > 0) {
        w *= PData / PMC;
    } else {
        return 0;
    }

    return w;
}

float EventInfoCreatorAlg ::get_double_tight_mu_weight(const xAOD::Muon *mu1,
                                                       const xAOD::Muon *mu2,
                                                       unsigned int runNumber) {
    float w = (*m_mu_ttva_sf_accessor)(*mu1) *
              (*m_mu_quality_tight_sf_accessor)(*mu1) *
              (*m_mu_iso_tight_sf_accessor)(*mu1) *
              (*m_mu_ttva_sf_accessor)(*mu2) *
              (*m_mu_quality_tight_sf_accessor)(*mu2) *
              (*m_mu_iso_tight_sf_accessor)(*mu2);

    float PMC = 0;
    float PData = 0;

    if (runNumber < 290000) {
        float effMC1 = (*m_mu_tight_trig_mc_2015_eff_accessor)(*mu1);
        float effMC2 = (*m_mu_tight_trig_mc_2015_eff_accessor)(*mu2);
        float effData1 = (*m_mu_tight_trig_data_2015_eff_accessor)(*mu1);
        float effData2 = (*m_mu_tight_trig_data_2015_eff_accessor)(*mu2);

        PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
        PData = effData1 * effData2 + effData1 * (1 - effData2) +
                (1 - effData1) * effData2;
    } else {
        float effMC1 = (*m_mu_tight_trig_mc_2016_eff_accessor)(*mu1);
        float effMC2 = (*m_mu_tight_trig_mc_2016_eff_accessor)(*mu2);
        float effData1 = (*m_mu_tight_trig_data_2016_eff_accessor)(*mu1);
        float effData2 = (*m_mu_tight_trig_data_2016_eff_accessor)(*mu2);

        PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
        PData = effData1 * effData2 + effData1 * (1 - effData2) +
                (1 - effData1) * effData2;
    }

    if (PMC > 0) {
        w *= PData / PMC;
    } else {
        return 0;
    }

    return w;
}

void EventInfoCreatorAlg ::initializeAccessors() {
    // Tight electron decorators
    m_tight_el1_pt_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el1_pt);
    m_tight_el1_phi_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el1_phi);
    m_tight_el1_eta_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el1_eta);
    m_tight_el1_charge_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el1_charge);
    m_tight_el1_index_accessor =
        std::make_unique<SG::AuxElement::Accessor<int>>(m_tight_el1_index);
    m_tight_el1_matched_accessor =
        std::make_unique<SG::AuxElement::Accessor<bool>>(m_tight_el1_matched);
    m_tight_el2_pt_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el2_pt);
    m_tight_el2_phi_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el2_phi);
    m_tight_el2_eta_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el2_eta);
    m_tight_el2_charge_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el2_charge);
    m_tight_el2_index_accessor =
        std::make_unique<SG::AuxElement::Accessor<int>>(m_tight_el2_index);
    m_tight_el2_matched_accessor =
        std::make_unique<SG::AuxElement::Accessor<bool>>(m_tight_el2_matched);
    m_tight_el_weight_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_el_weight);

    // Tight muon decorators
    m_tight_mu1_pt_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu1_pt);
    m_tight_mu1_phi_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu1_phi);
    m_tight_mu1_eta_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu1_eta);
    m_tight_mu1_charge_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu1_charge);
    m_tight_mu1_index_accessor =
        std::make_unique<SG::AuxElement::Accessor<int>>(m_tight_mu1_index);
    m_tight_mu1_matched_accessor =
        std::make_unique<SG::AuxElement::Accessor<bool>>(m_tight_mu1_matched);
    m_tight_mu2_pt_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu2_pt);
    m_tight_mu2_phi_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu2_phi);
    m_tight_mu2_eta_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu2_eta);
    m_tight_mu2_charge_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu2_charge);
    m_tight_mu2_index_accessor =
        std::make_unique<SG::AuxElement::Accessor<int>>(m_tight_mu2_index);
    m_tight_mu2_matched_accessor =
        std::make_unique<SG::AuxElement::Accessor<bool>>(m_tight_mu2_matched);
    m_tight_mu_weight_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_tight_mu_weight);

    // electron scale factors
    m_el_eff_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_el_eff_sf);
    m_el_id_tight_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_el_id_tight_sf);
    m_el_iso_tight_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_el_iso_tight_sf);
    m_el_tight_trig_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_el_tight_trig_sf);
    m_el_tight_trig_eff_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_el_tight_trig_eff);

    // muon scale factors
    m_mu_ttva_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_mu_ttva_sf);
    m_mu_quality_tight_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(
            m_mu_quality_tight_sf);
    m_mu_iso_tight_sf_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_mu_iso_tight_sf);
    m_mu_tight_trig_mc_2015_eff_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(
            m_mu_tight_trig_mc_2015_eff);
    m_mu_tight_trig_data_2015_eff_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(
            m_mu_tight_trig_data_2015_eff);
    m_mu_tight_trig_mc_2016_eff_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(
            m_mu_tight_trig_mc_2016_eff);
    m_mu_tight_trig_data_2016_eff_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(
            m_mu_tight_trig_data_2016_eff);

    // top reweighting
    m_top_weight_accessor =
        std::make_unique<SG::AuxElement::Accessor<float>>(m_top_weight_decoration);
}

void EventInfoCreatorAlg ::fillAccessors(xAOD::EventInfo *eventInfo) {
    (*m_tight_el1_pt_accessor)(*eventInfo) = -999.;
    (*m_tight_el1_phi_accessor)(*eventInfo) = -999.;
    (*m_tight_el1_eta_accessor)(*eventInfo) = -999.;
    (*m_tight_el1_charge_accessor)(*eventInfo) = -999.;
    (*m_tight_el1_index_accessor)(*eventInfo) = -999.;
    (*m_tight_el1_matched_accessor)(*eventInfo) = false;
    (*m_tight_el2_pt_accessor)(*eventInfo) = -999.;
    (*m_tight_el2_phi_accessor)(*eventInfo) = -999.;
    (*m_tight_el2_eta_accessor)(*eventInfo) = -999.;
    (*m_tight_el2_charge_accessor)(*eventInfo) = -999.;
    (*m_tight_el2_index_accessor)(*eventInfo) = -999.;
    (*m_tight_el2_matched_accessor)(*eventInfo) = false;
    (*m_tight_mu1_pt_accessor)(*eventInfo) = -999.;
    (*m_tight_mu1_phi_accessor)(*eventInfo) = -999.;
    (*m_tight_mu1_eta_accessor)(*eventInfo) = -999.;
    (*m_tight_mu1_charge_accessor)(*eventInfo) = -999.;
    (*m_tight_mu1_index_accessor)(*eventInfo) = -999.;
    (*m_tight_mu1_matched_accessor)(*eventInfo) = false;
    (*m_tight_mu2_pt_accessor)(*eventInfo) = -999.;
    (*m_tight_mu2_phi_accessor)(*eventInfo) = -999.;
    (*m_tight_mu2_eta_accessor)(*eventInfo) = -999.;
    (*m_tight_mu2_charge_accessor)(*eventInfo) = -999.;
    (*m_tight_mu2_index_accessor)(*eventInfo) = -999.;
    (*m_tight_mu2_matched_accessor)(*eventInfo) = false;

    (*m_tight_el_weight_accessor)(*eventInfo) = 0;
    (*m_tight_mu_weight_accessor)(*eventInfo) = 0;
}
