#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#


class TriggerList:

    # Single electron triggers
    el_single = []
    el_single += ['HLT_e24_lhmedium_L1EM20VH'] # 2015 only
    el_single += ['HLT_e60_lhmedium'] # 2015 only
    el_single += ['HLT_e120_lhloose'] # 2015 only
    el_single += ['HLT_e26_lhtight_nod0_ivarloose'] # 2016-
    el_single += ['HLT_e60_lhmedium_nod0'] # 2016-
    el_single += ['HLT_e140_lhloose_nod0'] # 2016-

    # Single muon triggers
    mu_single = []
    mu_single += ['HLT_mu20_iloose_L1MU15'] # 2015 only
    mu_single += ['HLT_mu26_ivarmedium'] # 2016-
    mu_single += ['HLT_mu50']

