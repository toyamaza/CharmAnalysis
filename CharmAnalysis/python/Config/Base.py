#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

def lumiCalcFiles(sampleType):
    list = []

    if sampleType == '2015' or sampleType == 'MC16a':
        list.append(
            'GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root'
        )

    if sampleType == '2016' or sampleType == 'MC16a':
        list.append(
            'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root'
        )

    if sampleType == '2017' or sampleType == 'MC16d':
        list.append(
            'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root'
        )

    if sampleType == '2018' or sampleType == 'MC16e':
        list.append(
            'GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root'
        )

    return list


def PRWConfigFiles(dataType, sampleType, derivationType):
    list = []

    if dataType == 'data':
        return list

    if sampleType == 'MC16d':
        list.append(
            'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'
        )
    if sampleType == 'MC16e':
        list.append(
            'GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'
        )

    if sampleType == 'MC16a':
        if derivationType == 'STDM13':
            list.append('CharmAnalysis/PRW/NTUP_PILEUP.STDM13.MC16a.root')
        else:
            list.append('CharmAnalysis/PRW/NTUP_PILEUP_MC16a.root')

    if sampleType == 'MC16d':
        if derivationType == 'STDM13':
            list.append('CharmAnalysis/PRW/NTUP_PILEUP.STDM13.MC16d.root')
        else:
            list.append('CharmAnalysis/PRW/NTUP_PILEUP_MC16d.root')

    if sampleType == 'MC16e':
        if derivationType == 'STDM13':
            list.append('CharmAnalysis/PRW/NTUP_PILEUP.STDM13.MC16e.root')
        else:
            list.append('CharmAnalysis/PRW/NTUP_PILEUP_MC16e.root')

    return list

# GRLs and LumiCalc files:
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2
def GRLFiles(sampleType):
    list = []
    if sampleType not in ['2015', '2016', '2017', '2018']:
        return list

    if sampleType == '2015':
        list.append(
            'GoodRunsLists/data15_13TeV/20170619/' +
            'data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml'
        )

    if sampleType == '2016':
        list.append(
            'GoodRunsLists/data16_13TeV/20180129/' +
            'data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml'
        )

    if sampleType == '2017':
        list.append(
            'GoodRunsLists/data17_13TeV/20180619/' +
            'data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml'
        )

    if sampleType == '2018':
        list.append(
            'GoodRunsLists/data18_13TeV/20190318/' +
            'data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml'
        )

    assert(len(list) == 1)

    return list
