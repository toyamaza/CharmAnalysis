#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

#
# conditions config
#

def setupConditions(svcMgr, athAlgSeq, include):

    # ConditionsTag
    from AthenaCommon.GlobalFlags  import globalflags
    from IOVDbSvc.CondDB import conddb
    if len(globalflags.ConditionsTag())!=0:
        conddb.setGlobalTag(globalflags.ConditionsTag())

    # Conditions Service for reading conditions data in serial and MT Athena
    from IOVSvc.IOVSvcConf import CondSvc
    svcMgr += CondSvc()

    # Conditions data access infrastructure for serial and MT Athena
    from IOVSvc.IOVSvcConf import CondInputLoader
    athAlgSeq += CondInputLoader("CondInputLoader")

    import StoreGate.StoreGateConf as StoreGateConf
    svcMgr += StoreGateConf.StoreGateSvc("ConditionStore")

    #
    # geometry and magnetic field
    #

    # Detector flags
    from AthenaCommon.DetFlags import DetFlags
    DetFlags.Forward_setOff()
    DetFlags.Calo_setOff()

    # Detector geometry and magnetic field
    from AtlasGeoModel import SetGeometryVersion
    from AtlasGeoModel import GeoModelInit
    from AtlasGeoModel import SetupRecoGeometry

    # MagneticField Service
    import MagFieldServices.SetupField

    # AtlasTrackingGeometrySvc: this line is expected to add it to svcMgr
    from TrkDetDescrSvc.AtlasTrackingGeometrySvc import AtlasTrackingGeometrySvc

    # Particle Property
    from AthenaCommon.Resilience import protectedInclude
    protectedInclude("PartPropSvc/PartPropSvc.py")
    include.block("PartPropSvc/PartPropSvc.py")

    # BeamCondSvc
    include("InDetBeamSpotService/BeamCondSvc.py")
