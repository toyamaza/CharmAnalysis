#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from ROOT import PATCore, xAOD
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool

from CharmAnalysis.Config import ContainerNames, TriggerList
from CharmAnalysis.Config import WorkingPoints
from CharmAnalysis.Config import formatBranchName

#
# Muon Algorithms config
#


def setupMuonAlgs(athAlgSeq, Conf):

    # Set the working point
    WP = WorkingPoints(Conf)

    # Include, and then set up the muon analysis algorithm sequence:
    from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
    seq = makeMuonAnalysisSequence(dataType=Conf.dataType,
                                   workingPoint='%s.%s' % (WP.mu_base_reco, WP.mu_base_isol),
                                   ptSelectionOutput=False,
                                   qualitySelectionOutput=False)

    # Adapt the pt cut
    seq.MuonPtCutAlg.selectionTool.minPt = WP.mu_pt_min
    seq.MuonPtCutAlg.selectionTool.maxPt = WP.mu_pt_max

    # Adapt the d0 cut
    seq.MuonTrackSelectionAlg.maxD0Significance = WP.mu_d0sig_max

    # calibration mode
    seq.MuonCalibrationAndSmearingAlg.calibrationAndSmearingTool.calibrationMode = 0

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex

    # Additional muon decoration
    alg = createAlgorithm('MuonDecoratorAlg', 'MuonDecoratorAlg')
    alg.primaryVertexKey = PVContainer
    alg.preselection = 'baselineSelection,as_char'
    alg.extraVars = Conf.extraVars
    seq.append(alg, inputPropName='muons',
               stageName='calibration')

    # muon quality decoration
    for id in WP.mu_id_list:
        makeMuonQualityAlg(seq, Conf.dataType, id)
        if Conf.dataType != 'data' and Conf.doPileupRW:
            makeMuonEfficiencyAlg(seq, Conf.dataType, id, 'Quality')

    # isolation decoration and eff
    for isol in WP.mu_isol_list:
        makeMuonIsolationAlg(seq, isol)
        if Conf.dataType != 'data' and Conf.doPileupRW:
            makeMuonEfficiencyAlg(seq, Conf.dataType, isol + 'Iso', 'Isol')

    # TTVA efficiency
    if Conf.dataType != 'data' and Conf.doPileupRW:
        makeMuonEfficiencyAlg(seq, Conf.dataType, 'TTVA')

    # trigger efficiency
    if Conf.dataType != 'data' and Conf.doPileupRW:
        for id in WP.mu_id_list:
            for (trig, min, max) in WP.mu_trig_list:
                makeMuonTriggerEfficiencyAlg(
                    seq, Conf.dataType, id + '.' + trig, min, max)

    # configure input/output
    seq.configure(inputName=ContainerNames.muons['input'],
                  outputName=ContainerNames.muons['output'] + 'Base_%SYS%')

    # Add the sequence to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def makeMuonQualityAlg(seq, dataType, wp):
    if wp == 'Tight':
        quality = xAOD.Muon.Tight
        pass
    elif wp == 'Medium':
        quality = xAOD.Muon.Medium
        pass
    elif wp == 'Loose':
        quality = xAOD.Muon.Loose
        pass
    elif wp == 'VeryLoose':
        quality = xAOD.Muon.VeryLoose
        pass
    elif wp == 'HighPt':
        quality = 4
        pass
    elif wp == 'LowPtEfficiency':
        quality = 5
        pass
    else:
        raise ValueError("invalid muon quality: \"" + wp +
                         "\", allowed values are Tight, Medium, Loose, " +
                         "VeryLoose, HighPt, LowPtEfficiency")

    alg = createAlgorithm('CP::AsgSelectionAlg',
                          'MuonQualitySelectionAlg' + wp)
    addPrivateTool(alg, 'selectionTool', 'CP::MuonSelectionTool')
    alg.selectionTool.MuQuality = quality
    qualitySelectionDecoration = 'isQuality_' + wp + ',as_char'
    alg.selectionDecoration = qualitySelectionDecoration
    seq.append(alg, inputPropName='particles',
               stageName='efficiency')


def makeMuonIsolationAlg(seq, isolation):
    alg = createAlgorithm('CP::MuonIsolationAlg',
                          'MuonIsolationAlg' + isolation)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'isolationTool', 'CP::IsolationSelectionTool')
    alg.isolationDecoration = 'isIsolated_' + isolation + ',as_char'
    alg.isolationTool.MuonWP = isolation
    seq.append(alg, inputPropName='muons',
               stageName='efficiency')


def makeMuonEfficiencyAlg(seq, dataType, wp, effType=''):
    alg = createAlgorithm('CP::MuonEfficiencyScaleFactorAlg',
                          'MuonEfficiencyScaleFactorAlg' + wp)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'efficiencyScaleFactorTool',
                   'CP::MuonEfficiencyScaleFactors')
    effSuffix = effType
    if effSuffix:
        effSuffix += '_'
    alg.scaleFactorDecoration = 'muon_effSF_' + effSuffix + wp.replace('Iso', '') + "_%SYS%"
    alg.outOfValidity = 2
    alg.outOfValidityDeco = 'bad_eff'

    alg.efficiencyScaleFactorTool.WorkingPoint = wp

    seq.append(alg, inputPropName='muons',
               stageName='efficiency')


def makeMuonTriggerEfficiencyAlg(seq, dataType, wp, minRun, maxRun):
    suffix = wp.replace('.', '_')

    alg = createAlgorithm('CP::MuonTriggerEfficiencyScaleFactorAlg',
                          'MuonEfficiencyScaleFactorAlg' + suffix)
    alg.preselection = 'baselineSelection,as_char'
    addPrivateTool(alg, 'efficiencyScaleFactorTool',
                   'CP::MuonTriggerScaleFactors')
    alg.mcEfficiencyDecoration = 'muon_effMC_Trig_' + suffix + "_%SYS%"
    alg.dataEfficiencyDecoration = 'muon_effData_Trig_' + suffix + "_%SYS%"
    alg.outOfValidity = 2
    alg.outOfValidityDeco = 'bad_eff'

    # Set the correct correction
    wps = wp.split('.')
    alg.efficiencyScaleFactorTool.MuonQuality = wps[0]
    alg.trigger = wps[1]
    alg.minRunNumber = minRun
    alg.maxRunNumber = maxRun

    seq.append(alg, inputPropName='muons',
               stageName='efficiency')


def getMuonBranches(Conf, PostProcessing):

    # Set the working point
    WP = WorkingPoints(Conf)

    branches = [
        '%SYS%.pt',
        '%SYS%.mu_selected_%SYS%'
    ]
    branches += [
        'NOSYS.is_bad',
        'NOSYS.eta',
        'NOSYS.phi',
        'NOSYS.charge',
        'NOSYS.d0sig',
        'NOSYS.d0',
        'NOSYS.z0sinTheta',
        'NOSYS.topoetcone20',
        'NOSYS.ptvarcone30_TightTTVA_pt1000',
        'NOSYS.ptvarcone30_TightTTVA_pt500',
        'NOSYS.neflowisol20',
    ]

    # selection
    for id in WP.mu_id_list:
        branches.append('%s.isQuality_%s' % ('%SYS%', id))
    for isol in WP.mu_isol_list:
        branches.append('%s.isIsolated_%s' % ('%SYS%', isol))

    # efficiencies
    if Conf.dataType != 'data' and Conf.doPileupRW:
        # reconstruction efficiency
        branches.append('%SYS%.muon_effSF_%SYS%')
        # TTVA efficiency
        branches.append('%SYS%.muon_effSF_TTVA_%SYS%')
        # quality efficiency
        for id in WP.mu_id_list:
            branches.append('%s.muon_effSF_Quality_%s_%s' % ('%SYS%', id, '%SYS%'))
        # isolation efficiency
        for isol in WP.mu_isol_list:
            branches.append('%s.muon_effSF_Isol_%s_%s' % ('%SYS%', isol, '%SYS%'))
        # trigger efficiency
        for id in WP.mu_id_list:
            for (trig, _, _) in WP.mu_trig_list:
                trigwp = id + '_' + trig
                branches.append('%s.muon_effMC_Trig_%s_%s' % ('%SYS%', trigwp, '%SYS%'))
                branches.append('%s.muon_effData_Trig_%s_%s' % ('%SYS%', trigwp, '%SYS%'))

    if PostProcessing and Conf.triggerChain == 'SingleLepton':
        # trigger matching
        triggers = TriggerList.mu_single
        for trig in triggers:
            branches.append('NOSYS.matched_%s' % trig)

    # truth
    if Conf.dataType != 'data' and Conf.doPileupRW:
        branches += [
            'NOSYS.truthType',
            'NOSYS.truthOrigin',
        ]

    # Extra variables
    if Conf.extraVars:
        branches += [
            'NOSYS.d0sigPV',
            'NOSYS.d0PV',
        ]

    return formatBranchName(ContainerNames.muons['output'], branches)
