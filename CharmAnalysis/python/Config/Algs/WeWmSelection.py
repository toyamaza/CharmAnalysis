#
# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaCommon.SystemOfUnits import GeV

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import formatBranchNameNoSYS

#
# WeWmSelection Algorithms config
#


def setupWeWmSelectionAlgs(athAlgSeq, Conf):
    # WeWmSelection analysis algorithm sequence
    from CharmAnalysis.WeWmSelectionSequence import makeWeWmSelectionSequence
    seq = makeWeWmSelectionSequence(dataType=Conf.dataType)
    seq.configure(
        inputName={'electrons': ContainerNames.electrons['output'] + "_NOSYS",
                   'muons': ContainerNames.muons['output'] + "_NOSYS",
                   'met': ContainerNames.MissingEnergy['output'] + "_NOSYS"},
        outputName=ContainerNames.WeWmSelection['output'])

    # Add all algorithms to the job:
    athAlgSeq += seq

    # Configure the alg
    seq.WeWmSelectionAlg.minMt = 60 * GeV
    seq.WeWmSelectionAlg.minEtmiss = 30 * GeV
    seq.WeWmSelectionAlg.minPt = 30 * GeV

    # Return to let the JO know this is active
    return seq


def getWeWmSelectionBranches():
    branches = [
        'pt',
        'charge',
        'phi',
        'm',
        'pdgId',
        'decayType',
    ]

    return formatBranchNameNoSYS(ContainerNames.WeWmSelection['output'], branches)
