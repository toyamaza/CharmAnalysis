#
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AthenaCommon import Logging
from CharmAnalysis.Config import WorkingPoints

#
# Track-jet clustering config
#
# copied from JetCommon.py to avoid nested imports that
# don't work outside the DerivationFramework
#

dfjetlog = Logging.logging.getLogger('JetCommon')
DFJetAlgs = {}


def addStandardJets(jetalg, rsize, inputtype, ptmin=0., ptminFilter=0.,
                    mods="default", calibOpt="none", ghostArea=0.01,
                    algseq=None, namesuffix="",
                    outputGroup="CustomJets", customGetters=None, pretools=[], constmods=[],
                    overwrite=False):
    jetnamebase = "{0}{1}{2}{3}".format(
        jetalg, int(rsize*10), inputtype, namesuffix)
    jetname = jetnamebase+"Jets"
    algname = "jetalg"+jetnamebase

    # return if the alg is already scheduled here :
    # from RecExConfig.AutoConfiguration import IsInInputFile
    if algseq is None:
        dfjetlog.warning("No algsequence passed! Will not schedule "+algname)
        return
    # elif IsInInputFile("xAOD::JetContainer", jetname) and not overwrite:
    elif False:
        dfjetlog.warning("Collection  "+jetname+" is already in input AOD!")
        return
    elif algname in DFJetAlgs:
        if hasattr(algseq, algname):
            dfjetlog.warning("Algsequence "+algseq.name() +
                             " already has an instance of "+algname)
        else:
            dfjetlog.info("Added "+algname+" to sequence "+algseq.name())
            algseq += DFJetAlgs[algname]
        return DFJetAlgs[algname]

    from JetRec.JetRecStandard import jtm

    if not jetname in jtm.tools:
        # no container exist. simply build a new one.
        # Set default for the arguments to be passed to addJetFinder
        defaultmods = {"EMTopo": "emtopo_ungroomed",
                       "LCTopo": "lctopo_ungroomed",
                       "EMPFlow": "pflow_ungroomed",
                       "EMCPFlow": "pflow_ungroomed",
                       "PFlowCustomVtx": "pflow_ungroomed",
                       "Truth": "truth_ungroomed",
                       "TruthWZ": "truth_ungroomed",
                       "PV0Track": "track_ungroomed",
                       "TrackCaloCluster": "tcc_ungroomed",
                       "UFOCSSK": "tcc_ungroomed",
                       "UFOCHS": "tcc_ungroomed",
                       }
        if mods == "default":
            mods = defaultmods[inputtype] if inputtype in defaultmods else []
        finderArgs = dict(modifiersin=mods, consumers=[])
        finderArgs['ptmin'] = ptmin
        finderArgs['ptminFilter'] = ptminFilter
        finderArgs['ghostArea'] = ghostArea
        finderArgs['modifiersin'] = mods
        finderArgs['calibOpt'] = calibOpt
        dfjetlog.info("mods in:" + str(finderArgs['modifiersin']))
        if overwrite:
            dfjetlog.info("Will overwrite AOD version of "+jetname)
            finderArgs['overwrite'] = True

        # finderArgs.pop('modifiersin') # leave the default modifiers.

        # map the input to the jtm code for PseudoJetGetter
        getterMap = dict(LCTopo='lctopo', EMTopo='emtopo', EMPFlow='empflow', EMCPFlow='emcpflow',
                         Truth='truth',  TruthWZ='truthwz', TruthDressedWZ='truthdressedwz', TruthCharged='truthcharged',
                         PV0Track='pv0track', TrackCaloCluster='tcc', UFOCSSK='ufocssk', UFOCHS='ufochs')

        # set input pseudojet getter -- allows for custom getters
        if customGetters is None:
            inGetter = getterMap[inputtype]
        else:
            inGetter = customGetters

        # create the finder for the temporary collection
        finderTool = jtm.addJetFinder(jetname, jetalg, rsize, inGetter, constmods=constmods,
                                      **finderArgs   # pass the prepared arguments
                                      )
        print("jtm")
        print(jtm)
        print("finderTool")
        print(finderTool)

        from JetRec.JetRecConf import JetAlgorithm
        # alg = JetAlgorithm(algname, Tools=pretools+[finderTool,jetCopyTool])
        alg = JetAlgorithm(algname, Tools=pretools+[finderTool])
        dfjetlog.info("Added "+algname+" to sequence "+algseq.name())
        algseq += alg
        DFJetAlgs[algname] = alg


def setupTrackJets(athAlgSeq, Conf):

    # TJ
    from JetRec.JetRecFlags import jetFlags
    jetFlags.useBTagging.set_Value_and_Lock(False)
    jetFlags.useCaloQualityTool.set_Value_and_Lock(False)
    jetFlags.usePFlow.set_Value_and_Lock(False)
    jetFlags.useTopo.set_Value_and_Lock(False)
    from JetRec.JetAlgorithm import addJetRecoToAlgSequence

    # Disable truh for data
    if Conf.dataType is 'data':
        jetFlags.useTruth.set_Value_and_Lock(False)

    # Modified locally to disable cluster origin calculation
    addJetRecoToAlgSequence(athAlgSeq, clusterOrigin=False)

    # Set the working point
    WP = WorkingPoints(Conf)

    # Create track-jet collections
    for r in WP.track_jet_radius:
        addStandardJets("AntiKt", r, "PV0Track", ptmin=5000,
                        algseq=athAlgSeq, ghostArea=0.0, namesuffix="Charm")


def setupTrackJetAlgs(athAlgSeq, Conf, jetRadius):

    # Track-jet sequence:
    seq = makeTrackJetAnalysisSequence(
        dataType=Conf.dataType, jetRadius=jetRadius)

    # Configure the sequence
    seq.configure(inputName={'jets': "AntiKt{}PV0TrackCharmJets".format(int(jetRadius * 10)),
                             'tracks': "AnalysisTracks"},
                  outputName={'jets': "AnalysisAntiKt{}PV0TrackJets".format(int(jetRadius * 10))})

    # Add all algorithms to the job:
    athAlgSeq += seq


def makeTrackJetAnalysisSequence(dataType, jetRadius):

    if dataType not in ['data', 'mc', 'afii']:
        raise ValueError("invalid data type: " + dataType)

    # Create the analysis algorithm sequence object:
    seq = AnaAlgSequence(
        'AntiKt{}TrackJetAnalysisSequence'.format(int(jetRadius * 10)))

    # Set up an algorithm that makes a view container
    alg = createAlgorithm('CP::AsgViewFromSelectionAlg',
                          'AntiKt{}TrackJetViewFromSelectionAlg'.format(int(jetRadius * 10)))
    seq.append(alg, inputPropName={'jets': 'input'},
               outputPropName={'jets': 'output'})

    # Track-jet decorator algorithm:
    alg = createAlgorithm('TrackJetDecoratorAlg',
                          'AntiKt{}TrackJetDecoratorAlg'.format(int(jetRadius * 10)))

    # Check for 'Loose' track WP
    alg.TrackSelectionTool.CutLevel = "Loose"

    # Add alg in sequence
    seq.append(alg, inputPropName={'jets': 'jets',
                                   'tracks': 'tracks'})

    # Return the sequence:
    return seq
