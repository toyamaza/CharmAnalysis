#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AthenaCommon.Constants import DEBUG, INFO, VERBOSE
from AthenaCommon.SystemOfUnits import GeV, MeV, mm
from CharmAnalysis.Config import ContainerNames, formatBranchNameNoSYS

#
# DSelection Algorithms config
#


def setupDSelectionAlgs(athAlgSeq, ToolSvc, Conf):
    # Track container name
    tracks = ContainerNames.tracks['output']
    if Conf.tracksFromTruth:
        tracks = ContainerNames.TruthTrackParticles

    # DSelection analysis algorithm sequence
    from CharmAnalysis.DSelectionSequence import makeDSelectionAnalysisSequence
    seq = makeDSelectionAnalysisSequence(dataType=Conf.dataType)
    seq.configure(inputName=tracks,
                  outputName={'composites': ContainerNames.DSelection['output'],
                              'composites_k3pi': ContainerNames.DSelection_k3pi['output'],
                              'composites_vee': ContainerNames.DSelection_vee['output']})

    # PV container name
    PVContainer = ContainerNames.PrimaryVertices if not Conf.fakePrimaryVertex else ContainerNames.FakePrimaryVertex
    seq.DSelectionAlg.primaryVertexKey = PVContainer

    if Conf.doV0Finding:
        # V0 Finder tools
        from CharmAnalysis.Config import V0FinderTools
        V0FinderTool = V0FinderTools.setupV0FinderTools(
            ToolSvc, TrackParticleCollection=ContainerNames.tracksV0['output'])

        # V0 Finder Algorithm
        from InDetV0Finder.InDetV0FinderConf import InDet__InDetV0Finder
        InDetV0Finder = InDet__InDetV0Finder(name='InDetV0Finder',
                                             decorateV0=True,
                                             InDetV0FinderToolName=ToolSvc.InDetV0FinderTool,
                                             V0ContainerName='V0Candidates',
                                             KshortContainerName='V0KshortVertices',
                                             LambdaContainerName='V0LambdaVertices',
                                             LambdabarContainerName='V0LambdabarVertices',
                                             VxPrimaryCandidateName=PVContainer)
        athAlgSeq += InDetV0Finder

    # Do Approximate Fit first
    seq.DSelectionAlg.doApproximateFit = Conf.doApproximateFit

    # Print level
    seq.DSelectionAlg.OutputLevel = INFO

    # Enable filtering
    seq.DSelectionAlg.enableFilter = False

    # Channels
    seq.DSelectionAlg.doDplus = True
    seq.DSelectionAlg.doDsubs = True
    seq.DSelectionAlg.doD0Kpi = True
    seq.DSelectionAlg.doD0K2pi = True
    seq.DSelectionAlg.doD0K3pi = Conf.d0K3pi
    seq.DSelectionAlg.doD0K3pi_only = Conf.d0K3pi_only
    if Conf.doV0Finding:
        seq.DSelectionAlg.doLambdaCLambdaPi = True
        seq.DSelectionAlg.doLambdaCKshortPi = True
        seq.DSelectionAlg.doKshortK = True
        seq.DSelectionAlg.doKshortPi = True

    # Truth Matcing
    seq.DSelectionAlg.doTruthMatching = True if not Conf.tracksFromTruth else False

    # Loose selection
    # Dplus
    seq.DSelectionAlg.MinMassDP = 1700 * MeV
    seq.DSelectionAlg.MaxMassDP = 2200 * MeV
    seq.DSelectionAlg.MaxfitChi2D0 = 50
    seq.DSelectionAlg.MinTrackPtDP = 500 * MeV
    seq.DSelectionAlg.MaxDPdRKPi = 1.0
    seq.DSelectionAlg.MinLxyDP = 0.5 * mm
    seq.DSelectionAlg.Maxd0D0_Kpi = 100 * mm
    seq.DSelectionAlg.MincosthetastarDP = -1
    seq.DSelectionAlg.MindMDP = 0 * MeV
    seq.DSelectionAlg.PhiwindowDP = 8 * MeV
    # Ds -> phi pi
    seq.DSelectionAlg.MinLxyDs = 0 * mm
    seq.DSelectionAlg.MincosthetastarDs = -1
    seq.DSelectionAlg.MaxcosthetastarDs = 1
    seq.DSelectionAlg.PhiwindowDs = 8 * MeV
    # D* -> D0pi
    seq.DSelectionAlg.MaxfitChi2D0 = 20
    seq.DSelectionAlg.MinTrackPtD0 = 500 * MeV
    seq.DSelectionAlg.MaxDRKPi = 1.0
    seq.DSelectionAlg.MinLxyD0 = 0 * mm
    seq.DSelectionAlg.Maxd0D0_Kpi = 100 * mm
    seq.DSelectionAlg.Maxd0D0_Kpipi0 = 100 * mm
    seq.DSelectionAlg.Maxd0PiSlow = 100 * mm
    seq.DSelectionAlg.MaxdRD0PiSlow = 10
    seq.DSelectionAlg.MaxMassD0_Kpi = 2000 * MeV
    seq.DSelectionAlg.MinMassD0_Kpi = 1800 * MeV
    seq.DSelectionAlg.MaxMassD0_Kpipi0 = 1800 * MeV
    seq.DSelectionAlg.MinMassD0_Kpipi0 = 1400 * MeV
    # Vee modes
    seq.DSelectionAlg.Maxd0LambdaCtoLambdaPi = 1.0 * mm
    seq.DSelectionAlg.Maxd0LambaCtoKsP = 1.0 * mm
    seq.DSelectionAlg.Maxd0DToKsK = 1.0 * mm
    seq.DSelectionAlg.Maxd0DToKsPi = 1.0 * mm
    seq.DSelectionAlg.MaxfitChi2LambdaC = 20.0
    seq.DSelectionAlg.MaxMassLambdaC = 2.5 * GeV
    seq.DSelectionAlg.MaxMassLambdaCin = 2.5 * GeV
    seq.DSelectionAlg.MincosthetastarLambdaC = -1.0
    seq.DSelectionAlg.MaxcosthetastarLambdaC = 1.0
    seq.DSelectionAlg.MinMassLambdaC = 2.00 * GeV
    seq.DSelectionAlg.MinMassLambdaCin = 1.95 * GeV
    seq.DSelectionAlg.MinTrackPtLambdaC = 500 * MeV
    seq.DSelectionAlg.MinPtLambaC = 5 * GeV
    seq.DSelectionAlg.MinLxyLambaCToLambdaPi = 0.0
    seq.DSelectionAlg.MinLxyLambdaCToKsP = 0.0
    seq.DSelectionAlg.MinLxyDToKsK = 0.0
    seq.DSelectionAlg.MinLxyDToKsPi = 0.0
    seq.DSelectionAlg.constrainV0 = True

    # TrkVKalVrtFitter
    from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
    DSelectionTrkVKalVrtFitter = Trk__TrkVKalVrtFitter(name="DSelectionTrkVKalVrtFitter",
                                                       Extrapolator="Trk::Extrapolator/InDetExtrapolator",
                                                       MakeExtendedVertex=True,
                                                       FirstMeasuredPoint=False)
    ToolSvc += DSelectionTrkVKalVrtFitter
    DSelectionTrkVKalVrtFitter.OutputLevel = INFO
    seq.DSelectionAlg.VKVrtFitter = DSelectionTrkVKalVrtFitter
    # constraint type: https://gitlab.cern.ch/atlas/athena/blob/21.2/Tracking/TrkVertexFitter/TrkVKalVrtFitter/src/SetFitOptions.cxx
    # -1) do not set it to any value (function call is omitted in c++)
    #  0) default (probably same as 1?)
    #  1) only mass constraint (no vertex constraint)
    #  2) exact pointing of summary track to vertex v0
    #  4) 2 + 1
    #  6) reconstructed vertex is close to v0 (Chi2 term)
    #  7) (summary_track-vertex_v0) Chi2 term. Both summary track and vertex covariances are used
    #  8) 7 + 1
    #  9) (summary_track-vertex_v0) Chi2 term. Only vertex covariance is used
    # 10) 9 + 1
    seq.DSelectionAlg.constraintType = -1

    # Add all algorithms to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getDSelectionBranches(Conf):
    branches = [
        'pt',
        'm',
        'eta',
        'phi',
        'pdgId',
        # 'charge',
        # 'ptcone20',
        # 'ptcone30',
        'ptcone40',
        'D0Index',
        'DeltaMass',
        'SlowPionD0',
        'SlowPionZ0SinTheta',
        'decayType',
        'truthBarcode',
        'fitOutput__Impact',
        'fitOutput__ImpactError',
        'fitOutput__ImpactZ0',
        'fitOutput__ImpactZ0Error',
        'fitOutput__ImpactTheta',
        'fitOutput__ImpactZ0SinTheta',
        'fitOutput__ImpactSignificance',
        'fitOutput__Charge',
        'fitOutput__Lxy',
        'fitOutput__LxyErr',
        'fitOutput__Chi2',
        'costhetastar',
        'mKpi1',
        'mKpi2',
        'mPhi1',
        'mPhi2',
        'daughterInfo__passLoose',
        'daughterInfo__passTight',
        'daughterInfo__truthMatchProbability',
        'daughterInfo__trackId',
        'daughterInfo__pdgId',
        'daughterInfo__pt',
        'daughterInfo__eta',
        'daughterInfo__phi',
        # daughterInfo__chi2',
        'daughterInfo__truthBarcode',
        'daughterInfo__truthDBarcode',
        'daughterInfo__z0SinTheta',
        'daughterInfo__z0SinThetaPV',
        'daughterInfo__d0',
        'daughterInfo__d0PV',
        'fitOutput__VertexPosition',
        # 'fitOutput__TrkAtVrt',
        # 'fitOutput__PerigeeCovariance',
        # 'fitOutput__Perigee',
        # 'fitOutput__ErrorMatrix',
        # 'fitOutput__Chi2PerTrack',
    ]

    output = formatBranchNameNoSYS(ContainerNames.DSelection['output'], branches)
    if Conf.d0K3pi:
        output += formatBranchNameNoSYS(ContainerNames.DSelection_k3pi['output'], branches)
    if Conf.doV0Finding:
        branches += ['V0Index']
        output += formatBranchNameNoSYS(ContainerNames.DSelection_vee['output'], branches)
    return output


def getVeeBranches():

    generic_container_names = [
        "V0Candidates",
    ]
    generic_branches = [
        'Kshort_mass',
        'Kshort_massError',
        'Lambda_mass',
        'Lambda_massError',
        'Lambdabar_mass',
        'Lambdabar_massError',
        'pT',
        'pTError',
        'Rxy',
        'RxyError',
        'px',
        'py',
        'pz',
    ]

    container_names = [
        "V0LambdaVertices",
        "V0LambdabarVertices",
        "V0KshortVertices",
    ]
    branches = [
        'mass',
        'pT',
        'pTError',
        'Rxy',
        'RxyError',
        'px',
        'py',
        'pz',
    ]

    output = []

    for container in generic_container_names:
        output += formatBranchNameNoSYS(container, generic_branches)

    for container in container_names:
        output += formatBranchNameNoSYS(container, branches)

    return output
