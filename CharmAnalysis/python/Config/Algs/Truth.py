#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from AthenaCommon.SystemOfUnits import GeV

from CharmAnalysis.Config import ContainerNames
from CharmAnalysis.Config import formatBranchNameNoSYS

#
# Truth Algorithms config
#


def setupTruthAlgs(athAlgSeq, Conf):

    # TruthSelection sequence:
    from CharmAnalysis.TruthSelectionSequence import makeTruthSelectionAnalysisSequence
    seq = makeTruthSelectionAnalysisSequence(dataType=Conf.dataType,
                                             truthJets=Conf.doTruthJets)

    # Configure the sequence
    seq.configure(inputName={'truth': ContainerNames.truth['input'],
                             'jets': 'AntiKt4TruthJets'},
                  outputName={'truth': ContainerNames.truth['output'],
                              'truthLep': ContainerNames.truthLep,
                              'jets': 'AnalysisAntiKt4TruthJets'})

    # Configuration
    seq.TruthSelectionAlg.MaxDEta = 2.6
    seq.TruthSelectionAlg.MinDPt = 5 * GeV
    seq.TruthSelectionAlg.MaxLepEta = 2.6
    seq.TruthSelectionAlg.MinLepPt = 20 * GeV

    # TRUTH derivation
    seq.TruthSelectionAlg.DressLeptons = Conf.dressLeptons
    seq.TruthSelectionAlg.DoTruthCharm = True
    seq.TruthSelectionAlg.DoTruthLeptons = Conf.doTruthLeptons
    seq.TruthSelectionAlg.TruthElectrons = ""
    seq.TruthSelectionAlg.TruthMuons = ""
    if Conf.sampleType == 'Truth':
        seq.TruthSelectionAlg.DressLeptons = True

    # Add all algorithms to the job:
    athAlgSeq += seq

    # Return to let the JO know this is active
    return seq


def getTruthBranches(Conf):
    branches = [
        'pt',
        'm',
        'eta',
        'phi',
        'barcode',
        'pdgId',
        'status',
        'vertexPosition',
        'cosThetaStarT',
        'LxyT',
        'ImpactT',
        'decayMode',
        'fromBdecay',
        'daughterInfoT__pdgId',
        'daughterInfoT__barcode',
        'daughterInfoT__pt',
        'daughterInfoT__eta',
        'daughterInfoT__phi',
    ]

    branches_lep = [
        'pt',
        'm',
        'e',
        'eta',
        'phi',
        'barcode',
        'pdgId',
        'status',
        'classifierParticleOrigin',
        'classifierParticleType',
    ]
    if Conf.dressLeptons:
        branches_lep += [
            'e_dressed',
            'eta_dressed',
            'phi_dressed',
            'pt_dressed',
        ]

    out = formatBranchNameNoSYS(ContainerNames.truth['output'], branches) + \
        formatBranchNameNoSYS(ContainerNames.truthLep, branches_lep)
    return out


def getTruthJets(Conf):
    branches = [
        'pt',
        'm',
        'eta',
        'phi',
        'HadronConeExclTruthLabelID',
        'HadronConeExclExtendedTruthLabelID',
        'ConeTruthLabelID',
        'ghostB_pt',
        'ghostB_eta',
        'ghostB_phi',
        'ghostB_m',
        'ghostB_pdgId',
        'ghostC_pt',
        'ghostC_eta',
        'ghostC_phi',
        'ghostC_m',
        'ghostC_pdgId',
    ]

    out = formatBranchNameNoSYS("AnalysisAntiKt4TruthJets", branches)
    return out
